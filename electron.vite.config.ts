import { resolve } from 'path'
import { defineConfig, externalizeDepsPlugin, bytecodePlugin } from 'electron-vite'
import vue from '@vitejs/plugin-vue'
import VueSetupExtend from 'vite-plugin-vue-setup-extend';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";

export default defineConfig({
  main: {
    plugins: [externalizeDepsPlugin(), bytecodePlugin()]
  },
  preload: {
    plugins: [externalizeDepsPlugin(), bytecodePlugin()]
  },
  renderer: {
    base: './',
    resolve: {
      alias: {
        // '@renderer': resolve('src/renderer'),
        '@': resolve('src/renderer'),
        '~': resolve('src/renderer/assets'),
      }
    },
    optimizeDeps: {
      include: ['schart.js']
    },
    plugins: [
      vue(),
      VueSetupExtend(),
      AutoImport({
        resolvers: [ElementPlusResolver()]
      }),
      Components({
        resolvers: [ElementPlusResolver()]
      }),
      createSvgIconsPlugin({
        // 指定目录(svg存放目录）
        iconDirs: [resolve('src/renderer/assets/svgs')],
        // 使用 svg 图标的格式（name为图片名称）
        symbolId: "icon-[name]",
        //生成组件插入位置 只有两个值 boby-last | body-first
        inject: 'body-last'
      })],
    define: {
      __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: "true",
    },
  }
})
