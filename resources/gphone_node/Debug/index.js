let gphone = require('./tinyWRAP_nodejs.node');
const http = require("http");
const assert = require("assert");

let sipService = new gphone.SipService();
sipService.setUserAgent("GPhoneNodejs")

// 获取指定 类型 设备列表
let devices = sipService.getMediaDevices("video");
console.log(devices);
// 设置使用的摄像头或麦克风名
let dev_index = 0;
sipService.setProducerFriendlyName("video", devices[dev_index]);
console.log(sipService.getProducerFriendlyName("video"));
assert(devices[dev_index] == sipService.getProducerFriendlyName("video"));

sipService.start();
var readline = require('readline')
const rl = readline.createInterface({
    input:process.stdin,
    output:process.stdout
})

let volume = 50;
let mutea = false;
let mutev = false;
let use_camera_index = 0;

let remoteVideoListener = new gphone.RemoteVideoListener((width, height, fps, frameSize, frameData) => {
    // console.log("receive data width=" + width + ", height=" + height + ", fps=" + fps + ", frameSize=" + frameSize);
});
let localVideoListener = new gphone.LocalVideoListener((width, height, fps, frameSize, frameData) => {
    // console.log("local data width=" + width + ", height=" + height + ", fps=" + fps + ", frameSize=" + frameSize);
});

let debug_cb = new gphone.DebugCallback((level, msg) => {
    console.log(level + " " + msg);
});
sipService.setDebugCallback(debug_cb);
sipService.setStackListener(new gphone.SipStackListener((state) => {
    console.log("sip stack changed " + state);
}));

sipService.setRegistrationListener(new gphone.SipRegistrationListener((state) => {
    console.log("sip registration changed " + state);
}));


sipService.setInviteListener(new gphone.SipInviteListener((state, user, mediaType) => {
    console.log("sip invite changed " + state + ", user" + user + ", mediaType=" + mediaType);
    if (state == 'INCOMING') {
        let avSession = sipService.accept(remoteVideoListener, localVideoListener);
    }
}));

rl.on('line',function(line) {
    if (line == 'call') {
        sipService.makeCall("1012", "audio_video", remoteVideoListener, localVideoListener);
    }

    if (line == 'hungup') {
        sipService.hangup();
    }

    if(line == 'camera') {
        sipService.setProducerInputSource("camera", true, devices[use_camera_index ]);
        use_camera_index = (use_camera_index == 0 ? 1 : 0);
    } else if (line == 'live') {
        sipService.setProducerInputSource("live", true, "rtsp://admin:Nucleus!@10.8.6.130");
    } else if (line == 'mp4') {
        sipService.setProducerInputSource("mp4", true, "genew.mp4");
    } else if (line == 'image') {
        sipService.setProducerInputSource("image", true, "logo.jpeg");
    }else if (line == 'screencast') {
        sipService.setProducerInputSource("screencast", true, "");
    } else if (line == 'mutea') {
        mutea = !mutea;
        sipService.mute("audio", mutea);
    } else if (line == 'mutev') {
        mutev = !mutev;
        sipService.mute("video", mutev);
    } else if (line == 'volume+') {
        sipService.setVolume(volume++);
    } else if (line == 'volume-') {
        sipService.setVolume(volume--);
    } else if (line == 'reg') {

        sipService.regist("10.8.19.122", 5065, "52788", "Nucleus0755");

        // 编码格式
        let codecs = ["PCMA", "PCMU", "H264HP"]
        sipService.setCodecs(codecs);
        assert.deepEqual(codecs, sipService.getCodecs());
    } else if (line == 'ureg') {
        sipService.stop();
    }else if (line == 'crash') {
        sipService.crash();
    }
})

sipService.regist("10.8.19.122", 5065, "52788", "Nucleus0755");

// 编码格式
let codecs = ["PCMA", "PCMU", "H264BP", "H264MP", "H264HP"]
sipService.setCodecs(codecs);
assert.deepEqual(codecs, sipService.getCodecs());

sipService.setProducerInputSource("live", true, "rtsp://admin:Nucleus!@10.8.6.130");

rl.on('close',function(){
    process.exit()
})

// 分辨率
let pref_size = '720P';
sipService.setPrefSize(pref_size);
// assert(pref_size == sipService.getPrefSize());

// sipService.regist("10.8.19.124", 5060, "42890", "123456");

// 使用 mp4，live(rtsp/rtmp) 文件作为音视频输入,
// 参数1：输入类型：camera, mp4, live, image
// 参数2：是否使用外部音频作为输入
// 参数1：URL, MP4，image时参数为文件绝对地址，live时，为"rtsp://admin:Nucleus!@10.8.6.130", camera时可为空
// sipService.setProducerInputSource("live", true, "rtsp://admin:Nucleus!@10.8.6.130");

let source = sipService.getProducerInputSource();
console.log("************************* " + source.type + " " + source.enable_audio + " " + source.path);


//assert(pref_size == sipService.getPrefSize());

// http.createServer(function (request, response) {
//     // 发送 HTTP 头部  // HTTP 状态值: 200 : OK // 内容类型: text/plain
//     response.writeHead(200, {'Content-Type': 'text/plain'});
//     // 发送响应数据 "Hello World"
//     response.end('Hello World\n' + sipService.getStackState());
// }).listen(8888);

// sipService.stop();

// while (true) {
// }