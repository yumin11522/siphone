## 1、GPhone for Electron addon

- 支持 electron v13.1.8版本
### Version 1.0.3
`svn: 13150 18:21:45, 2023年5月5日`
- 支持VoLTE视频通话
- 支持FFmpeg硬件解码能力
- 支持H264 HP编解码

### Version 1.0.2
`svn: 12974 16:06:28, 2023年2月20日`

- 支持设置：JPEG图片、RTSP/RTMP摄像头、MP4文件,桌面共享，及摄像头作为音视频输入源

### Version 1.0.1
`svn: 12645 18:21:14, 2022年9月2日`

- 支持指定使用摄像头或麦克风设备
- 修复若干 Bug

### Version 1.0.0 
- 基本呼叫接口如：注册、注销、音视频呼叫、挂断、拒接等。