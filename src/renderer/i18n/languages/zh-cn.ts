
export const lang = {
  welcome: "欢迎进入本框架",
  buttonTips: "您可以点击的按钮测试功能",
  waitDataLoading: "等待数据读取",
  about: {
    system: "关于系统",
    language: "语言：",
    languageValue: "中文简体",
    currentPagePath: "当前页面路径：",
    currentPageName: "当前页面名称：",
    vueVersion: "Vue版本：",
    electronVersion: "Electron版本：",
    nodeVersion: "Node版本：",
    systemPlatform: "系统平台：",
    systemVersion: "系统版本：",
    systemArch: "系统位数：",
  },
  labels: {
    number: "SIP号码",
    passwd: "密码"
  },
  tips: {
    number: "请输入SIP号码"
  }
};
