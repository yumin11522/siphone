import { Menus } from '@/types/menu';

let index = 0;
export const menuData: Menus[] = [
    {
        id: '0',
        title: '呼叫',
        index: '/call',
        icon: 'Grid',
    },
    {
        id: '1',
        title: '通话记录',
        index: '/clogs',
        icon: 'Clock',
    },
    {
        id: '2',
        title: '通讯录',
        index: '/contact',
        icon: 'UserFilled',
    },
    {
        id: '3',
        title: '系统设置',
        index: '/dashboard',
        icon: 'Setting',
        children: [
            {
                id: '31',
                title: 'SIP设置',
                index: '/setting/sip',
                icon: 'HomeFilled',
            },
            {
                id: '32',
                title: 'H323设置',
                index: '/setting/h323',
                icon: 'HomeFilled',
            },
            {
                id: '33',
                title: '基础设置',
                index: '/setting/base',
                icon: 'Calendar',
            },
            {
                id: '34',
                title: '媒体设置',
                index: '/setting/media',
                icon: 'Calendar',
            },
            {
                id: '35',
                title: '主题',
                index: '/theme',
                icon: 'Brush',
            },
            {
                id: '36',
                title: '关于',
                index: '/about',
                icon: 'QuestionFilled',
            }
        ]
    },
    
];
