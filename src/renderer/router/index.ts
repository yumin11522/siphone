import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Main from '@/views/main.vue'
import Call from '@/views/pages/call.vue'
import { title } from 'process'

const routes: RouteRecordRaw[] = [
    // 空hash，则跳转至Login页面
    { path: '/', redirect: '/call' },
    // 精确匹配 #/login，指向Login页面
    {
        path: '/', 
        name: 'Main',
        component: Main, 
        children: [
            {
                path: '/call',
                name: 'call',
                meta: {
                    title: '呼叫',
                    permiss: '8'
                },
                component: Call
            },
            {
                path: '/clogs',
                name: 'clogs',
                meta: {
                    title: '呼叫记录',
                    permiss: '8'
                },
                component: () => import('../views/pages/clogs.vue'),
            },
            {
                path: '/contact',
                name: 'contact',
                meta: {
                    title: '通讯录',
                    permiss: '8'
                },
                component: () => import('../views/pages/contact.vue'),
            },
            {
                path: '/theme',
                name: 'theme',
                meta: {
                    title: '主题设置',
                    permiss: '7',
                },
                component: () => import('../views/pages/theme.vue'),
            },
            {
                path: '/setting/base',
                name: 'base',
                meta: {
                    title: '基础设置',
                    permiss: '7',
                },
                component: () => import('../views/setting/base.vue'),
            },
            {
                path: '/setting/sip',
                name: 'sip',
                meta: {
                    title: 'SIP设置',
                    permiss: '7',
                },
                component: () => import('../views/setting/sip.vue'),
            },
            {
                path: '/setting/h323',
                name: 'h323',
                meta: {
                    title: 'H323',
                    permiss: '7',
                },
                component: () => import('../views/setting/h323.vue'),
            },
            {
                path: '/setting/media',
                name: 'media',
                meta: {
                    title: '音、视频设置',
                    permiss: '7',
                },
                component: () => import('../views/setting/media.vue'),
            },
            {
                path: '/about',
                name: 'about',
                meta: {
                    title: '关于',
                    permiss: '7',
                },
                component: () => import('../components/system-info-mation.vue'),
            }
        ]
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

export default router