import { contextBridge } from 'electron'
import { electronAPI } from '@electron-toolkit/preload'
import h323 from '../main/h323-api'
import sip from '../main/gphone-api'

// Custom APIs for renderer
const api = {h323: h323, sip: sip}

// Use `contextBridge` APIs to expose Electron APIs to
// renderer only if context isolation is enabled, otherwise
// just add to the DOM global.
if (process.contextIsolated) {
  try {
    contextBridge.exposeInMainWorld('electron', electronAPI)
    contextBridge.exposeInMainWorld('api', api)
  } catch (error) {
    console.error(error)
  }
} else {
  // @ts-ignore (define in dts)
  window.electron = electronAPI
  // @ts-ignore (define in dts)
  window.api = api
}
