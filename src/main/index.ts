import { app, shell, BrowserWindow, ipcMain, Tray, Menu, globalShortcut, crashReporter } from 'electron'
import path, { join } from 'path'
import { electronApp, optimizer, is } from '@electron-toolkit/utils'
import icon from '../../resources/icon.ico?asset'

import creatWorker from './http-server?nodeWorker'

import { getVersion } from './version'

// 获取奔溃堆栈文件存放路径
let crashFilePath = '';
let crashDumpsDir = '';
try {
  // electron 低版本
  crashFilePath = path.join(app.getPath('temp'), app.getName() + ' Crashes');
  console.log('————————crash path:', crashFilePath);

  // electron 高版本
  crashDumpsDir = app.getPath('crashDumps');
  console.log('————————crashDumpsDir:', crashDumpsDir);
} catch (e) {
  console.error('获取奔溃文件路径失败', e);
}

// 开启crash捕获
crashReporter.start({
  productName: 'NuMaxPhone',
  companyName: 'NuMaxPhone',
  submitURL: 'http://10.8.106.111:1800',  // 上传到服务器的地址
  // submitURL: 'https://portal.numax.tech/numaxphone/crash',  // 上传到服务器的地址
  uploadToServer: true, // 不上传服务器
  ignoreSystemCrashHandler: false, // 不忽略系统自带的奔溃处理，为 true 时表示忽略，奔溃时不会生成奔溃堆栈文件
});

let mainWindow
let browserWinState = false
let tray
let timer = null
let worker

function createWindow(): void {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 900,
    height: 670,
    show: false,
    autoHideMenuBar: true,
    // titleBarStyle: 'hidden',
    ...(process.platform === 'linux' ? { icon } : { icon }),
    // resizable: false,//可否缩放
    webPreferences: {
      preload: join(__dirname, '../preload/index.js'),
      sandbox: false,
      devTools: true,
      nodeIntegration: true, // 在网页中集成Node
      nodeIntegrationInWorker: true,
      contextIsolation: false
    }
  })

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
  })

  mainWindow.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url)
    return { action: 'deny' }
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(join(__dirname, '../renderer/index.html'))
  }

  mainWindow.webContents.on('did-finish-load', function (/* event */) {
    mainWindow.webContents.send('crash-file-path', `${crashFilePath} or ${crashDumpsDir}`);
  });
  /** 1.1 关闭主窗口监听 */
  mainWindow.on('close', (e) => {
    e.preventDefault();  // 阻止退出程序
    mainWindow.setSkipTaskbar(true)   // 取消任务栏显示
    mainWindow.hide();    // 隐藏主程序窗口
    browserWinState = true
  })
  /** 1.2 创建托盘 */
  if (!!!tray) {
    createMainTray();
  }
  /** 1.3 主程序加载就绪 */
  mainWindow.once('ready-to-show', () => {                // 页面准备好了才show, 避免出现页面加载的过程.
    mainWindow.show()
  })
  /** 程序奔溃时 */
  mainWindow.webContents.on('crashed', (/* event */) => {
    mainWindow.close();
    mainWindow = null
    createWindow();
  })
  mainWindow.on('plugin-crashed', (/* e */)=>{
    console.log("======================================================================")
  })
}

/** 2. 自定义托盘 */
function createMainTray() {
  tray = new Tray(icon)

  // 自定义托盘图标的内容菜单
  let getMenuItems = () => {
    if (process.platform === 'linux') {

      return [{
        label: '退出', click: () => {
          worker.postMessage('gphoneExit')
          mainWindow.destroy()
          app.quit()
        },
      }, {
        label: '打开', click: () => {
          mainWindow.setSkipTaskbar(false)
          mainWindow.show();
          browserWinState = false
          if (timer) {
            tray.setImage(icon)
            clearInterval(timer)
          }
        },
      }
      ]
    } else {
      return [
        {
          label: '退出', click: () => {
            // 退出窗口之前先释放SIP
            worker.postMessage('gphoneExit')
            mainWindow.destroy()
            app.quit()
          },
        },
      ]
    }
  }

  const contextMenu = Menu.buildFromTemplate(getMenuItems())
  tray.setToolTip('NuMax Phone')  // 设置鼠标指针在托盘图标上悬停时显示的文本
  tray.setContextMenu(contextMenu)  // 设置图标的内容菜单
  // 点击托盘图标，显示主窗口
  tray.on("click", () => {
    mainWindow.setSkipTaskbar(false)
    mainWindow.show();
    browserWinState = false
    if (timer) {
      tray.setImage(icon)
      clearInterval(timer)
    }
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // Set app user model id for windows
  electronApp.setAppUserModelId('com.genew.numax.numaxphone')

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  // IPC test
  ipcMain.on('ping', () => console.log('pong'))

  createWindow()

  worker = creatWorker({ workerData: 'worker' })
  worker.on('message', (message) => {
    console.log(`\nMessage from worker: ${message}`)
  })
  worker.postMessage('')

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

// 注册快捷键
app.on('ready', async () => {
  globalShortcut.register('F11', function () {
    mainWindow.webContents.openDevTools();
  })
})

//控制台
ipcMain.on('openDevTool', function () {
  mainWindow.webContents.openDevTools();
})

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
// 以上英文注释是Electron官方的main.js注释，意思是通过require()来引入其他代码文件。
// 但是由于Vite的机制，打包的时候会忽略require()，因此只能通过import方式来引入外部方法。
ipcMain.handle('getVersion', getVersion)