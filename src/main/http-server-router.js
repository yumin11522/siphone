import { Router } from 'express'
import gphone from './gphone-api';
import h323 from './h323-api';
import { getSipInfo } from './http-client'

const router = new Router()

router.get('/', (req, resp) => {
    resp.send('NuMax Phone Version ' + gphone.version());
});

router.post('/gphone/cmd/sip', (req, resp) => {
    const nuasSchemeHostPort = req.body['nuasSchemeHostPort']
    const nuasToken = req.body['nuasToken']
    getSipInfo(nuasSchemeHostPort, nuasToken, (is_ok, res) => {
        console.log(res)
        if (!is_ok) {
            resp.send({ 'code': -1, 'cause': '从AS ' + nuasSchemeHostPort + ' 获取SIP信息失败!' + res });
        } else {
            resp.send({ 'code': 0, 'data': { 'user': res['account'] } });
        }
    });
});

router.post('/gphone/cmd/register', (req, resp) => {
    const nuasSchemeHostPort = req.body['nuasSchemeHostPort']
    const nuasToken = req.body['nuasToken']
    getSipInfo(nuasSchemeHostPort, nuasToken, (is_ok, res) => {
        console.log(res)
        if (!is_ok) {
            resp.send({ 'code': -1, 'registState': false, 'cause': '从AS ' + nuasSchemeHostPort + ' 获取SIP信息失败!' + res });
        } else {
            gphone.regist(res.ip, res.port, res.account, res.password)
            resp.send({ 'code': 0, 'registState': true, 'data': { 'user': res['account'] } });
        }
    });
});

router.post('/gphone/cmd/unregister', (req, resp) => {
    const user = req.body['user'] || '';
    const ret = gphone.unregist(user);
    resp.send({ 'code': 0, 'user': user, 'unregistState': 0 });
});

router.post('/gphone/cmd/makeCall', (req, resp) => {
    const user = req.body['user'];
    const type = req.body['type'];
    const ret = gphone.makeCall(user, type);
    resp.send({ 'code': 0, 'user': user, 'result': ret });
});

router.post('/gphone/cmd/hangupCall', (req, resp) => {
    const ret = gphone.hangup();
    resp.send({ 'code': 0, 'result': ret });
});

router.get('/crash', (req, resp) => {
    gphone.crash()
});

router.post('/gphone/config/getPrefSize', (req, resp) => {
    resp.send({ 'code': 0, 'data': { 'prefSize': gphone.getPrefSize() } });
});

router.post('/gphone/config/setPrefSize', (req, resp) => {
    resp.send({ 'code': 0, 'data': { 'prefSize': gphone.getPrefSize() } });
});

router.post('/gphone/config/getCodecs', (req, resp) => {
    resp.send({ 'code': 0, 'data': { 'codecs': ['H264BP', 'PCMA'] } });
});

router.post('/gphone/config/setCodecs', (req, resp) => {
    console.log(req.body.codecs)
    gphone.setCodecs(req.body.codecs.split(','));
    resp.send({ 'code': 0, });
});

router.post('/gphone/config/setProducerInputSource', (req, resp) => {
    gphone.setProducerInputSource(req.body.type, req.body.enable_audio, req.body.path)
    resp.send({ 'code': 0, });
});


router.post('/gphone/config/getProducerInputSource', (req, resp) => {
    const src = gphone.getProducerInputSource()

    resp.send({ 'code': 0, 'data': { 'enable_audio': src.enable_audio, 'type': src.type, 'path': src.type } });
});

router.post('/h323/cmd/makeCall', (req, resp) => {
    const user = req.body['user'];
    const type = req.body['type'];
    const ret = h323.makeCall(user, type);
    resp.send({ 'code': 0, 'user': user, 'result': ret });
});

router.post('/h323/cmd/hangupCall', (req, resp) => {
    const ret = h323.hangup();
    resp.send({ 'code': 0, 'result': ret });
});


const remoteRouter = new Router()
const localRouter = new Router()

export {router, remoteRouter, localRouter}
