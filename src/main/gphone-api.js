class SipClient {
    static getInstance() {
        if (SipClient.instance) {
            return SipClient.instance;
        } else {
            return SipClient.instance = new SipClient();
        }
    }

    constructor() {
        console.log("+++++++++++++++++++++++ 1 ++++++++++++++++++++++++++ \n");
        /** 猜想：path.resolve 拿到的是动态库的“绝对路径”，而动态库又依赖如avutil.dll库，
        *   需要使用相对路径才能加载到第三方库（avutil.dll)，暂时改为相对路径方式
        */
        // const wrap = process.env.NODE_ENV === 'development'
        //     ? (process.platform === 'linux'
        //         ? require('../../../gphone_node/Debug/tinyWRAP_nodejs_linux.node')
        //         : require('../../../gphone_node/Debug/tinyWRAP_nodejs.node'))
        //     : (process.platform === 'linux'
        //         ? require(path.resolve(__dirname, '../../../extraResources/gphone_node/Debug/tinyWRAP_nodejs_linux.node'))
        //         : require('../../../gphone_node/Debug/tinyWRAP_nodejs.node'));
        if (process.platform === 'win32') {
            this.wrap = require('../../resources/gphone_node/Debug/tinyWRAP_nodejs.node');
        } else if (process.platform === 'linux') {
            this.wrap = require('../../resources/gphone_node/Debug/tinyWRAP_nodejs_linux.node');
        }
        const wrap = this.wrap;
        this.sipService = new wrap.SipService();

        // this.DebugCallback = wrap.DebugCallback;
        // this.SipStackListener = wrap.SipStackListener;
        // this.SipRegistrationListener = wrap.SipRegistrationListener;
        // this.SipInviteListener = wrap.SipInviteListener
        // this.RemoteVideoListener = wrap.RemoteVideoListener
        // this.LocalVideoListener = wrap.LocalVideoListener
    }

    setUserAgent(userAgent) {
        this.sipService.setUserAgent(userAgent);
    }

    setDebugCallback(cb) {
        this._debugCb = new this.DebugCallback((level, msg) => {
            _logger.info(level + "--------------------------------------" + msg)
        });
        this.sipService.setDebugCallback(this._debugCb);
    }

    setStackListener(cb) {
        this._stackListener = new this.wrap.SipStackListener((state) => {
            console.log("Sip stack state change" + state);
            cb(state);
        });
        this.sipService.setStackListener(this._stackListener);
    }

    setRegistrationListener(cb) {
        this._regListener = new this.wrap.SipRegistrationListener((state) => {
            if (cb) {
                console.log("Sip registeration state change" + state);
                cb(state);
            }
        });
        this.sipService.setRegistrationListener(this._regListener);
    }

    setInviteListener(cb) {
        this._inviteListener = new this.wrap.SipInviteListener((state, user, mediaType) => {
            console.log("Sip invite state change" + state);
            cb(state, user, mediaType);
        });
        this.sipService.setInviteListener(this._inviteListener);
    }

    start() {
        this.sipService.setProducerInputSource("mp4", true, "f:/genew.mp4");
        return this.sipService.start();
    }

    gphoneStop() {
        return this.sipService.stop();
    }

    stop() {
        return this.sipService.stop();
    }

    regist(ip, port, number, password) {
        return this.sipService.regist(ip, port, number, password);
    }

    unregist() {
        return this.sipService.unregist();
    }

    setRemoteVideoListener(cb) {
        this._remoteVideoCB = cb;
    }

    setLocalVideoListener(cb) {
        this._localVideoCB = cb;
    }

    accept(remoteVideoCb, localVideoCb) {
        this._remoteVideoCB = remoteVideoCb || this._remoteVideoCB;
        this._localVideoCB = localVideoCb || this._localVideoCB;
        this._remoteVideoListener = new this.wrap.RemoteVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._remoteVideoCB) {
                this._remoteVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this._localVideoListener = new this.wrap.LocalVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._localVideoCB) {
                this._localVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this.sipService.accept(this._remoteVideoListener, this._localVideoListener);
    }

    makeCall(user, type, remoteVideoCb, localVideoCb) {
        this._remoteVideoCB = remoteVideoCb || this._remoteVideoCB;
        this._localVideoCB = localVideoCb || this._localVideoCB;
        this._remoteVideoListener = new this.wrap.RemoteVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._remoteVideoCB) {
                this._remoteVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this._localVideoListener = new this.wrap.LocalVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._localVideoCB) {
                this._localVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this.sipService.makeCall(user, type, this._remoteVideoListener, this._localVideoListener);
    }

    hangup() {
        return this.sipService.hangup();
    }

    isRegisted() {
        return this.sipService.isRegisted();
    }

    reject() {
        return this.sipService.reject();
    }

    mute(type, state) {
        return this.sipService.mute(type, state);
    }
    setVolume(value) {
        return this.sipService.setVolume(value);
    }
    sendDtmf(value) {
        return this.sipService.sendDtmf(value);
    }

    setPrefSize(preSize) {
        return this.sipService.setPrefSize(preSize);
    }

    getPrefSize() {
        return this.sipService.getPrefSize();
    }

    setCodecs(codecs) {
        return this.sipService.setCodecs(codecs);
    }
    setProducerInputSource(type, audioOutSide, path) {
        return this.sipService.setProducerInputSource(type, audioOutSide, path);
    }
    getMediaDevices(type) {
        return this.sipService.getMediaDevices(type);
    }

    setProducerFriendlyName(type, name) {
        return this.sipService.setProducerFriendlyName(type, name);
    }

    version() {
        return this.sipService.version();
    }

    getLocalAddresses() {
        return this.sipService.getLocalAddresses();
    }

    setLocalIp(localIp) {
        return this.sipService.setLocalIp(localIp);
    }

    crash() {
        this.sipService.crash()
    }
}


const gphone = SipClient.getInstance();
export default gphone;