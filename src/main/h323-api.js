class H323Client {
    static getInstance() {
        if (H323Client.instance) {
            return H323Client.instance;
        } else {
            console.log(" New H323Client ")
            return H323Client.instance = new H323Client();
        }
    }

    constructor() {
        console.log("+++++++++++++++++++++++ 1 ++++++++++++++++++++++++++ \n");
        /** 猜想：path.resolve 拿到的是动态库的“绝对路径”，而动态库又依赖如avutil.dll库，
        *   需要使用相对路径才能加载到第三方库（avutil.dll)，暂时改为相对路径方式
        */
        // const wrap = process.env.NODE_ENV === 'development'
        //     ? (process.platform === 'linux'
        //         ? require('../../../gphone_node/Debug/tinyWRAP_nodejs_linux.node')
        //         : require('../../../gphone_node/Debug/tinyWRAP_nodejs.node'))
        //     : (process.platform === 'linux'
        //         ? require(path.resolve(__dirname, '../../../extraResources/gphone_node/Debug/tinyWRAP_nodejs_linux.node'))
        //         : require('../../../gphone_node/Debug/tinyWRAP_nodejs.node'));
        if (process.platform === 'win32') {
            this.wrap = require('../../resources/gphone_node/Debug/h323_node.node');
        } else if (process.platform === 'linux') {
            this.wrap = require('../../resources/gphone_node/Debug/h323_node_linux.node');
        }
        const wrap = this.wrap;
        this._h323Client = new wrap.H323Client();

        this.CallListener = wrap.CallListener;
        this.RemoteVideoListener = wrap.RemoteVideoListener
        this.LocalVideoListener = wrap.LocalVideoListener
    }

    setCallListener(cb) {
        this._callListener = new this.wrap.CallListener((state, user, mediaType) => {
            console.log("Sip invite state change" + state);
            cb(state, user, mediaType);
        });
        this._h323Client.setCallListener(this._callListener);
    }

    start() {
        console.debug("------------------------------");
        return this._h323Client.start();
    }

    h323Stop() {
        return this._h323Client.stop();
    }

    stop() {
        return this._h323Client.stop();
    }

    setRemoteVideoListener(cb) {
        this._remoteVideoCB = cb;
    }

    setLocalVideoListener(cb) {
        this._localVideoCB = cb;
    }

    accept(remoteVideoCb, localVideoCb) {
        this._remoteVideoCB = remoteVideoCb || this._remoteVideoCB;
        this._localVideoCB = localVideoCb || this._localVideoCB;
        this._remoteVideoListener = new this.wrap.RemoteVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._remoteVideoCB) {
                this._remoteVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this._localVideoListener = new this.wrap.LocalVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._localVideoCB) {
                this._localVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this._h323Client.accept(this._remoteVideoListener, this._localVideoListener);
    }

    makeCall(user, type, remoteVideoCb, localVideoCb) {
        this._remoteVideoCB = remoteVideoCb || this._remoteVideoCB;
        this._localVideoCB = localVideoCb || this._localVideoCB;
        this._remoteVideoListener = new this.wrap.RemoteVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._remoteVideoCB) {
                this._remoteVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this._localVideoListener = new this.wrap.LocalVideoListener((width, height, fps, frameSize, frameData) => {
            if (this._localVideoCB) {
                this._localVideoCB(width, height, fps, frameSize, frameData);
            }
        });
        this._h323Client.makeCall(user, type, this._remoteVideoListener, this._localVideoListener);
    }

    hangup() {
        return this._h323Client.hangup();
    }

    reject() {
        return this._h323Client.reject();
    }
}
const h323 = H323Client.getInstance();
export default h323;