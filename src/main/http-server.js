import express from 'express';
import expressWs from 'express-ws';
import bodyParser from 'body-parser';
import gphone from './gphone-api';
import h323 from './h323-api';

import { router, remoteRouter, localRouter } from './http-server-router';

import { parentPort, workerData, isMainThread } from 'worker_threads'
// 监听来自主线程的消息
const port = parentPort
if (!port) throw new Error('IllegalState')

port.on('message', (msg) => {
    port.postMessage(`hello ${workerData} ================== ${isMainThread}`)
    if (msg === 'gphoneExit') {
        gphone.stop()
    }
})


const SIP_ROOT_NAME = '/gphone';
const CMD_PORT = 8848;
const VIDEO_REMOTE_PORT = 8849;
const cmdSrv = express();

expressWs(cmdSrv);

cmdSrv.use(bodyParser.urlencoded({ extended: false }))
cmdSrv.use(bodyParser.json())

//设置跨域访问
cmdSrv.all("*", function (req, res, next) {
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin", "*");
    //允许的header类型
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options')
        res.send(200);  //让options尝试请求快速结束
    else
        next();
})

router.get(SIP_ROOT_NAME + "/cmd/port", (req, resp) => {
    resp.send({
        code: 0, 'data': {
            'rest-cmd-port': CMD_PORT,
            'video-remote-port': VIDEO_REMOTE_PORT,
            'video-local-port': VIDEO_LOCAL_PORT,
        }
    });
});

cmdSrv.use(router);

cmdSrv.ws(SIP_ROOT_NAME + "/cmd", (ws, ret) => {
    let firstLoad;

    gphone.setStackListener((state) => {
        ws.send(JSON.stringify({ 'cmd': 'EVENT_STACK_STATE', 'state': state }));
    });
    gphone.setRegistrationListener((state) => {
        ws.send(JSON.stringify({ 'cmd': 'EVENT_USER_STATE', 'state': state }));
    });
    gphone.setInviteListener((state, user, mediaType) => {
        ws.send(JSON.stringify({ 'cmd': 'EVENT_CALL_STATE', 'state': state, 'mediaType': mediaType, 'user': user }));
    });

    gphone.start();

    ws.on('close', () => {
        gphone.stop();
    });
});

/**
 * H323 消息
 */
cmdSrv.ws("/h323/cmd", (ws, ret) => {
    h323.start();
    h323.setCallListener((state, user, mediaType) => {
        ws.send(JSON.stringify({ 'cmd': 'EVENT_CALL_STATE', 'state': state, 'mediaType': mediaType, 'user': user }));
    });
    console.log("---- start -------------")
    ws.on('close', () => {
        h323.stop();
    });
});


cmdSrv.listen(CMD_PORT, "127.0.0.1", () => {
    console.log("Cmd service listen on " + CMD_PORT);
});


function fill_array(array, offset, intValue) {
    array[offset] = ((intValue >> 24) & 0xFF)
    array[offset + 1] = ((intValue >> 16) & 0xFF)
    array[offset + 2] = ((intValue >> 8) & 0xFF)
    array[offset + 3] = ((intValue >> 0) & 0xFF)
}

function initSegment(width, height) {
    let data = new Uint8Array(16);
    fill_array(data, 0, 0x88888888);
    fill_array(data, 4, width);
    fill_array(data, 8, height);
    fill_array(data, 12, 0);
    return data;
}

const remoteVideoWsSrv = express();
expressWs(remoteVideoWsSrv)
remoteVideoWsSrv.use(remoteRouter);

remoteVideoWsSrv.ws(SIP_ROOT_NAME + "/data/remote", (ws, ret) => {
    let firstLoad;
    gphone.setRemoteVideoListener((width, height, fps, frameSize, frameData) => {
        if (!firstLoad) {
            firstLoad = true;
            console.log("first load " + width + " height " + height)
            ws.send(initSegment(width, height));
        }
        ws.send(frameData);
    });

    ws.on('close', () => {
        firstLoad = false;
    });
});

remoteVideoWsSrv.ws(SIP_ROOT_NAME + "/data/local", (ws, ret) => {
    let firstLoad;
    gphone.setLocalVideoListener((width, height, fps, frameSize, frameData) => {
        if (!firstLoad) {
            firstLoad = true;
            console.log("first load " + width + " height " + height)
            ws.send(initSegment(width, height));
        }
        ws.send(frameData);
    });

    ws.on('close', () => {
        firstLoad = false;
    });
    ws.on('open', () => {
        console.log("************************************************* \n");
    });
});

remoteVideoWsSrv.ws("/h323/data/remote", (ws, ret) => {
    let firstLoad;
    h323.setRemoteVideoListener((width, height, fps, frameSize, frameData) => {
        if (!firstLoad) {
            firstLoad = true;
            console.log("first load " + width + " height " + height)
            ws.send(initSegment(width, height));
        }
        ws.send(frameData);
    });

    ws.on('close', () => {
        firstLoad = false;
    });
});

remoteVideoWsSrv.ws("/h323/data/local", (ws, ret) => {
    let firstLoad;
    h323.setLocalVideoListener((width, height, fps, frameSize, frameData) => {
        if (!firstLoad) {
            firstLoad = true;
            console.log("first load " + width + " height " + height)
            ws.send(initSegment(width, height));
        }
        ws.send(frameData);
    });

    ws.on('close', () => {
        firstLoad = false;
    });
    ws.on('open', () => {
        console.log("************************************************* \n");
    });
});

remoteVideoWsSrv.listen(VIDEO_REMOTE_PORT, "127.0.0.1", () => {
    console.log("Remote video websocket listen on " + VIDEO_REMOTE_PORT);
});
