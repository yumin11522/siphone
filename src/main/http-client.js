// import http from 'http'

// var http = require("http");
// var https = require("https");
// const crypto = require('crypto')

import http from 'http'
import https from 'https'
import crypto from 'crypto'

// import dns from 'node:dns'
// import https from 'https'

function httpGet(url, headers, callback) {
    // step1,创建一个 http.ClientRequest
    const options = {
        method: "GET",
        headers: headers
    };

    const isSecure = url.startsWith('https') || url.startsWith('HTTPS');
    const req = (isSecure ? https : http).request(url, options, function (incoming_msg) {
        // 监听IncomingMessage的data事件，当收到服务器发过来的数据的时候，触发这个事件
        incoming_msg.on("data", function (data) {
            if (incoming_msg.statusCode === 200) {
                const ret = JSON.parse(data);
                if (ret.status.code === 0) {
                    callback(true, ret.data);
                } else {
                    callback(false, ret.status.desc);
                }
            }
        });
    });

    // 把这个请求发送出去
    req.end();
}

// post可以带body数据传到服务器
function httpPost(url, headers, body, callback) {
    const options = {
        method: "POST",
        headers: headers
    };

    const isSecure = url.startsWith('https') || url.startsWith('HTTPS');
    const req = (isSecure ? https : http).request(url, options, function (incoming_msg) {
        console.log("respones status " + incoming_msg.statusCode);

        // 监听IncomingMessage的data事件，当收到服务器发过来的数据的时候，触发这个事件
        incoming_msg.on("data", function (data) {
            if (incoming_msg.statusCode === 200) {
                callback(true, data);
            }
        });

    });

    // step2 写入body数据
    if (body !== '') {
        req.write(body);
    }

    // 发送请求
    req.end();
}

function getNuasHttpHeaders(toekn) {
    const shasum = crypto.createHash('sha1')
    const timestamp = new Date().getTime();
    const headers = {
        'X-Token': toekn,
        "X-Nonce": '' + timestamp,
        'X-Signature': '' + shasum.update(timestamp + '' + timestamp + 'gns11529c136998cb6').digest('hex'),
        'X-Timestamp': '' + timestamp,
        // 'X-Encrypt-Type': 'encrypt',
        'X-Visitor': 'NuClient4Cpp',
        'Accept-Encoding': 'zip',
        'X-Client-Version': 'NuClient4Cpp',
        // 'Content-Type': 'application/json;charset=UTF-8',
    };
    return headers;
}

// httpPost('https://portal.numax.tech/nuas/api/v1/sip?type=NuClient4Cpp', headers, '', function (is_ok, data) {
//     if (is_ok) {
//         console.log("upload_success", data.toString());
//     }
// });
function getSipInfo(nuasSchemeHostPort, token, cb) {
    return httpGet(nuasSchemeHostPort + '/nuas/api/v1/sip?type=NuClient4Cpp', getNuasHttpHeaders(token), cb)
}

export { getSipInfo }