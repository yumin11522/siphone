<h1 align="center">NuMax Phone</h1>

<p align="center">一款跨平台，支持高清SIP、H323呼叫客户端</p>


<p align='center'>
<img src='./build/numaxphone.png'/>
</p>

## 特性
- 支持 SIP 
- SIP 音、视频呼叫
- 外部音视频源作为输入（RTMP, 图片，MP4）
- 最高支持全高清（1080p）视频
- VoLTE
- 语音呼叫 (G729AB 1、AMR-NB、iLBC、GSM、PCMA、PCMU、Speex-NB)
- 视频通话（VP8、H264、MP4V-ES、Theora、H.263、H.263-1998、H.261）

## 待开发特性
- 通讯录
- 通话记录
